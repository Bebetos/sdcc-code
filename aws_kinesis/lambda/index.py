import boto3
import json
import base64
import os
import io

from botocore.exceptions import ClientError
from influxdb import InfluxDBClient
from time import strftime


def lambda_handler(event, context):
    for record in event['Records']:

        payload = base64.b64decode(record['kinesis']['data']).decode("utf-8")
        rec = json.loads(payload)

        # There is no message: just insert record to db
        if not rec['message']:
            # Insert into db
            insert_influxdb(rec)
        # There is a message: create a log file  -> insert record to db
        else:
            # Create log file
            insert_influxdb(rec)
            create_log_file(rec['id'], rec['message'])

    return 'Successfully processed {} records.'.format(len(event['Records']))


def create_log_file(id, msg):
    """
    This function is used for create a log file stored into a S3 bucket.
    The log file is a plain text file contains the id of the car in the first row and the message on the second.

    :param id: The id of the car
    :param msg: The message
    :return: nothing
    """
    ACCESS_KEY = os.environ['ACCESS_KEY']
    SECRET_KEY = os.environ['SECRET_KEY']
    REGION_NAME = os.environ['REGION_NAME']
    BUCKET_CAR_NOTIFICATION = os.environ['BUCKET_CAR_NOTIFICATION']

    path = '/tmp/'
    filename = str(id) + '_' + strftime("%Y-%m-%dT%H%M%SZ") + ".log"

    g = open(path + filename, 'w')
    g.write(str(id))
    g.write("\n")
    g.write(str(msg))
    g.close()

    s3_conn = boto3.resource('s3', aws_access_key_id=ACCESS_KEY,
                             aws_secret_access_key=SECRET_KEY)

    s3_conn.Bucket(BUCKET_CAR_NOTIFICATION).upload_file(path + filename, filename)


def insert_influxdb(record):
    """
    This function is used for insert the record to influxDB.

    :param record: The record that is going to be insert into influxDB
    :return: nothing
    """
    DB_ADDRESS = os.environ['DB_ADDRESS']
    DB_PORT = os.environ['DB_PORT']
    DB_USER = os.environ['DB_USER']
    DB_PASSWORD = os.environ['DB_PASSWORD']
    DB_NAME = os.environ['DB_NAME']

    client = InfluxDBClient(DB_ADDRESS, DB_PORT,
                            DB_USER, DB_PASSWORD, DB_NAME)

    client.create_database(DB_NAME)

    json_body = [
        {
            "measurement": "car",
            "tags": {
                "id_car": record['id']
            },
            "fields": {
                "speed": record['speed'],
                "consumption": record['consumption'],
                "time": record['time'],
                "message": record['message'],
                "position.latitude": record['position']['latitude'],
                "position.longitude": record['position']['longitude']
            }
        }
    ]

    client.write_points(json_body)