import constant
from entity.Record import Record
import boto3
import json
from time import sleep


if __name__ == "__main__":

    my_stream_name = 'car2lambda'

    kinesis_client = boto3.client('kinesis', region_name='eu-west-1',
                                  aws_access_key_id=constant.ACCESS_KEY,
                                  aws_secret_access_key=constant.SECRET_KEY)


    while True:

        car = Record(1,0,0,0,0,"")
        car2 = Record(2,0,0,0,0,"")

        # Generazione dei record
        car.generate_record()
        car2.generate_record()

        # Inserisco i record dentro lo stream
        kinesis_client.put_record(StreamName=my_stream_name,
                                  Data=json.dumps(car.dict),
                                  PartitionKey="PK-car1")

        kinesis_client.put_record(StreamName=my_stream_name,
                                  Data=json.dumps(car2.dict),
                                  PartitionKey="PK-car2")

        sleep(1)