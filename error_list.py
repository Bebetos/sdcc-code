"""
    ERRORS
"""
ERR_EMAIL_USED = "Email already used! Please sign in!"
ERR_404 = "Ops! Error 404"
ERR_UPDATEPASSWORD = "Ops! Can't update the password!"

"""
    MESSAGES
"""
OK_REGISTRATION = "Sign up success! Login with your information."
OK_NEWCAR = "Success! Your car has been correctly added!"
OK_UPDATECAR = "Success! Your informations has been updated correctly!"
OK_UPDATEPASSWORD = "Success! Your password has been updated correctly!"