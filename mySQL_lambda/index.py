import json
import sql_rest
import time

def respond(err, res=None):
    return {
        'statusCode': '400' if err else '200',
        'body': err.message if err else json.dumps(res),
        'headers': {
            'Content-Type': 'application/json',
        },
    }

def lambda_handler(event, context):
    
    path = event['path']
    
    if event['httpMethod'] == 'POST':
        body = json.loads(event['body'])
        
        # Delete a car
        if path == '/car/delete':
            id_car = body['id_car']

            result = sql_rest.deleteCar(id_car)
            return respond(None, result)
        
        
        # Insert new car
        elif path == '/car/save':
            id = body['id']
            brand = body['brand']
            model = body['model']
            car_plate = body['car_plate']

            result = sql_rest.addCar(brand, model, car_plate, id)
            return respond(None, result)
        
        
        # Update car information 
        elif path == '/car/update':
            id_car = body['id_car']
            brand = body['brand']
            model = body['model']
            car_plate = body['car_plate']

            result = sql_rest.updateCar(id_car, brand, model, car_plate)
            return respond(None, result)
          
            
        # Sign up
        elif path == '/session/signup':
            email = body['email']
            password = body['password']
            phone = 'na'
            name = body['name']
            timezone = 'na'

            result = sql_rest.createAccount(email, password, phone, timezone, name)

            return respond(None, result)
        
        
        # Update profile informations
        elif path == '/profile/updateinfo':
            id = body['id']
            name = body['name']
            phone = body['phone']
            timezone = body['timezone']

            result = sql_rest.updateAccount(id, name, phone, timezone)
            return respond(None, result)
        
        
        # Update password
        elif path == '/profile/updatepsw':
            id = body['id']
            psw = body['password']
            oldpsw = body['old_password']
            
            millis = int(round(time.time() * 1000))
            result = sql_rest.updatePassword(id, psw, oldpsw)
            return respond(None, result)
        
        
    if event['httpMethod'] == 'GET':
        
        # Get all the cars
        if path == '/car/getall':
            id = event['queryStringParameters']['id']

            millis = int(round(time.time() * 1000))
            result = sql_rest.getCars(id)
            return respond(None, result)

        
        # Get profile informations
        elif path == '/profile/getinfo':
            id = event['queryStringParameters']['id']

            result = sql_rest.getInformations(id)
            return respond(None, result)


        # Update password
        elif path == '/profile/updatepsw':
            id = event['queryStringParameters']['id']
            psw = event['queryStringParameters']['password']
            oldpsw = event['queryStringParameters']['old_password']

            result = sql_rest.updatePassword(id, psw, oldpsw)
            return respond(None, result)


        # Sign in
        elif path == '/session/login':
            email = event['queryStringParameters']['email']
            password = event['queryStringParameters']['password']

            result = sql_rest.getID(email, password)
            return respond(None, result)


    else:
        result = {"err_code": 404, "message": "Resource not found!"}
        return respond(None, result)
