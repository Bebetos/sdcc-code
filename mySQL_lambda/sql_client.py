"""
    This module is used for the connection with the MySQL database.
"""
import pymysql
import os

class MySQLClient:

    
    def __init__(self):
        self.ip_address = os.environ['IP_ADDRESS']
        self.user = os.environ['USER']
        self.password = os.environ['PASSWORD']
        self.db_name = os.environ['DB_NAME']
        self.charset = 'utf8mb4'
        self.connection = None

    
    def getCursor(self):
        """
        This function is used for get the connection and the cursor to the database.
        
        :return: The cursor on success, an error message on failure
        """
        if(self.connection == None):

            try:
                self.connection = pymysql.connect(host=self.ip_address,
                                          user=self.user,
                                          password=self.password,
                                          db=self.db_name,
                                          charset=self.charset,
                                          cursorclass=pymysql.cursors.DictCursor,
                                          autocommit=True)

                curs = self.connection.cursor()
                return curs
            except pymysql.Error as e:
                return str(e)
        else:
            return self.connection.cursor()


    def doCommit(self):
        """
        Wrapper for the commit().
        :return: Nothing
        """
        self.connection.commit()


    def doRollback(self):
        """
        Wrapper for the rollback().
        :return: Nothing
        """
        self.connection.rollback()


    def closeConnection(self):
        """
        Wrapper for the close().
        :return: Nothing
        """
        try:
            self.connection.close()
        except pymysql.Error as e:
            return str(e)
            
    
    def messageResponse(self, error_code, msg=None, data=None):
        """
        This function is a utility for create a JSON message for the response.

        :param error_code: A code
        :param message: The message
        :return: An object ready to be a JSON {'err_code': value, 'message': value}
        """
        return {
            'err_code': error_code,
            'message': msg,
            'data': data
        }