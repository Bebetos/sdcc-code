"""
    This module is dedicated for the API REST with MySQL database.
    It's used for manage account (creation) and cars (CRUD).
"""

import sql_client
import pymysql
import error_list as err


"""
+---------------------------------------+
|   ACCOUNT SECTION                     |
+---------------------------------------+
"""
def createAccount(email, password, phone, timezone, name=None):
    """
    This function is used for create the account.
    
    :param email: A string with the email address
    :param password: A string with the password
    :param phone: A string with the cellphone number
    :param name: A name for the account, can be None
    :return: A message with the information of success or failure
    """

    client = sql_client.MySQLClient()
    cursor = client.getCursor()

    if checkEmail(email):
        return client.messageResponse(201, err.ERR_EMAIL_USED)

    if name is None:
        try:
            query = "INSERT INTO account(email, password, phone, timezone)" \
                    " VALUES (%s, %s, %s, %s)"
            cursor.execute(query, (email, password, phone, timezone))
            client.doCommit()
            client.closeConnection()
            return client.messageResponse(200, msg=err.OK_REGISTRATION)

        except:
            client.doRollback()
            client.closeConnection()
            return client.messageResponse(404, msg=err.ERR_404)

    else:
        try:
            query = "INSERT INTO account(email, password, phone, name, timezone)" \
                    " VALUES (%s, %s, %s, %s, %s)"
            cursor.execute(query, (email, password, phone, name, timezone))
            client.doCommit()
            client.closeConnection()
            return client.messageResponse(200, msg=err.OK_REGISTRATION)

        except:
            client.doRollback()
            client.closeConnection()
            return client.messageResponse(404, msg=err.ERR_404)


def updateAccount(id, name, phone, timezone):
    """
    This function is used for update the account informations.
    
    :param id: The session ID.
    :param name: The field name on the database.
    :param phone: The phone number.
    :return: A message of success or failure.
    """
    client = sql_client.MySQLClient()
    curs = client.getCursor()

    query = "UPDATE account SET name=%s, phone=%s, timezone=%s WHERE id=%d"

    try:
        curs.execute(query, (name, phone, timezone, id))
        client.doCommit()
        client.closeConnection()
        return client.messageResponse(200, msg=err.OK_UPDATECAR)
    except:
        client.doRollback()
        client.closeConnection()
        return client.messageResponse(404, msg=err.ERR_404)


def getInformations(id):
    """
    This function is used for get all the account informations.
    
    :param id: The session ID.
    :return: A list with all the informations.
    """

    client = sql_client.MySQLClient()
    curs = client.getCursor()

    int_id = int(id)

    try:
        query = "SELECT name, phone, timezone FROM account WHERE id=%d" % int(id)

        curs.execute(query)
        result = curs.fetchone()
        client.closeConnection()

        data = {
                "name": result['name'],
                "phone": result['phone'],
                "timezone": result['timezone']
            }
        return data
    except pymysql.Error as e:
        client.closeConnection()
        print(str(e))
        return client.messageResponse(404, msg=err.ERR_404)


def updatePassword(id, new_password, old_password):
    """
    This function is used for update the password.
    
    :param id: The session ID.
    :param new_password: The new password.
    :return: A messagge of success or failure.
    """

    client = sql_client.MySQLClient()
    curs = client.getCursor()

    query = "UPDATE account SET password='%s' WHERE id=%d AND password='%s'" % (new_password, id, old_password)
            
    try:
        result = curs.execute(query)
        client.closeConnection()
        if not result:
            return client.messageResponse(201, msg=err.ERR_UPDATEPASSWORD)
        else:
            return client.messageResponse(200, msg=err.OK_UPDATEPASSWORD)

    except pymysql.Error as e:
        client.closeConnection()
        return client.messageResponse(404, msg=err.ERR_404)


def checkEmail(email):
    """
    This function is used for check if the email already exists in the database.
    
    :param email: A string with the email
    :return: True if exist, False otherwise
    """

    client = sql_client.MySQLClient()
    curs = client.getCursor()

    query = "SELECT * FROM account WHERE email=%s"
    curs.execute(query, (email,))

    if curs.fetchone() is None:
        client.closeConnection()
        return False
    else:
        client.closeConnection()
        return True


def getPhone(id_car):
    """
    This function is used for get the phone number. Used for a notification send.
    
    :param id_car: The id of the car
    :return: The phone number on success, an error message on failure 
    """

    client = sql_client.MySQLClient()
    curs = client.getCursor()

    #TODO change query
    query = "SELECT DISTINCT phone FROM account INNER JOIN car " \
            "ON account.id = car.account_id WHERE account.id = %d"

    try:
        curs.execute(query, (id_car,))
        result = curs.fetchone()
        client.closeConnection()
        data = {"phone": result[0]}
        return client.messageResponse(200, data=data)
    except:
        client.closeConnection()
        return client.messageResponse(404, msg=err.ERR_404)


def getID(email, password):
    """
    This function is used for the log in.
    
    :param email: The string with the email.
    :param password: The string with the password.
    :return: The ID used for the session.
    """
    client = sql_client.MySQLClient()
    curs = client.getCursor()

    query = "SELECT id FROM account WHERE account.email = %s AND account.password = %s"

    
    curs.execute(query, (email, password))
    result = curs.fetchone()
    client.closeConnection()
    if result == None:
        if(checkEmail(email)):
            return client.messageResponse(201, msg=err.ERR_WRONG)
        else:
            return client.messageResponse(202, msg=err.ERR_404)
    else:
        data = {"id": result['id']}
        return client.messageResponse(200, data=data)


"""
+---------------------------------------+
|   CAR SECTION                         |
+---------------------------------------+
"""
def addCar(brand, model, car_license_plate, account_id):
    """
    This function is used for the creation of a new car.
    
    :param brand: A string with the brand name
    :param model: A string with the model name
    :param car_license_plate: A string with the car license plate
    :param account_id: The id for the account that own the car
    :return: A message with the information of success or failure
    """

    client = sql_client.MySQLClient()
    curs = client.getCursor()

    query = "INSERT INTO car(`brand`, `model`, `car_license_plate`, `account_id`)" \
            " VALUES('%s', '%s', '%s', '%d')" % (brand, model, car_license_plate, account_id)

    try:
        curs.execute(query)
        client.doCommit()
        client.closeConnection()
        return client.messageResponse(200, msg=err.OK_NEWCAR)
    except pymysql.Error as e:
        client.doRollback()
        client.closeConnection()
        print(str(e))
        return client.messageResponse(404, msg=err.ERR_404)


def getCars(account_id):
    """
    This function is used for get all the cars owned by the account.
    
    :param account_id: The id of the account
    :return: A list of cars on success, an error message on failure
    """
    client = sql_client.MySQLClient()
    curs = client.getCursor()

    query = "SELECT car.id, car.brand, car.model, car.car_license_plate " \
            "FROM car INNER JOIN account ON car.account_id = account.id " \
            "WHERE car.account_id = %d" % int(account_id)

    try:
        curs.execute(query)
        result = curs.fetchall()

        cars = []
        for row in result:
            car = {
                "id":row["id"],
                "brand": row["brand"],
                "model": row["model"],
                "car_license_plate": row["car_license_plate"]
            }
            cars.append(car)

        client.closeConnection()
        data = {"cars": cars}
        return client.messageResponse(200, data=data)
    except:
        client.closeConnection()
        return client.messageResponse(404, msg=err.ERR_404)


def updateCar(id, brand, model, car_license_plate):
    """
    This function is used for update the infromation about a car.
    
    :param id: The id of the car
    :param brand: A string with the brand name
    :param model: A string with the model name
    :param car_license_plate: A string with the car license plate
    :return: A message with the information of success or failure
    """
    client = sql_client.MySQLClient()
    curs = client.getCursor()

    query = "UPDATE car SET brand='%s', model='%s', car_license_plate='%s' " \
            "WHERE id=%d " % (brand, model, car_license_plate, id)

    try:
        curs.execute(query)
        client.doCommit()
        client.closeConnection()
        return client.messageResponse(200, msg=err.OK_NEWCAR)
    except:
        client.doRollback()
        client.closeConnection()
        return client.messageResponse(404, msg=err.ERR_404)


def deleteCar(id):
    """
    This function is used for delete a car.
    
    :param id: The id of the car.
    :return: A message with the information of success or failure
    """
    client = sql_client.MySQLClient()
    curs = client.getCursor()

    query = "DELETE FROM car " \
            "WHERE id=%d " % int(id)

    try:
        curs.execute(query)
        client.doCommit()
        client.closeConnection()
        return client.messageResponse(200, msg=err.OK_NEWCAR)
    except:
        client.doRollback()
        client.closeConnection()
        return client.messageResponse(404, msg=err.ERR_404)


"""
    UTILITY FOR DB SERVER
"""

"""
#TODO Generator for token session
def id_generator(size=16, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))
"""


def createDB():
    """
    This function is used for create the DB on the AWS RDS instance
    :return: A message of success or failure.
    """

    client = sql_client.MySQLClient()
    curs = client.getCursor()

    query0 = "CREATE SCHEMA IF NOT EXISTS `car_notify` DEFAULT CHARACTER SET utf8"

    query1 = "CREATE TABLE IF NOT EXISTS `car_notify`.`account` ( "\
            "`id` INT NOT NULL AUTO_INCREMENT," \
            "`email` VARCHAR(45) NOT NULL, "\
            "`password` VARCHAR(20) NOT NULL, " \
            "`phone` VARCHAR(45) NOT NULL, " \
            "`name` VARCHAR(45) NULL, "\
            "PRIMARY KEY (`id`))"

    query2 = "CREATE TABLE IF NOT EXISTS `car_notify`.`car` ( "\
             "`id` INT NOT NULL AUTO_INCREMENT," \
             "`brand` VARCHAR(45) NOT NULL, "\
             "`model` VARCHAR(45) NOT NULL, "\
             "`car_license_plate` VARCHAR(45) NOT NULL, "\
             "`account_id` INT NOT NULL, "\
             "PRIMARY KEY (`id`, `account_id`), "\
             "INDEX `fk_car_account_idx` (`account_id` ASC), "\
             "CONSTRAINT `fk_car_account` "\
             "FOREIGN KEY (`account_id`) "\
             "REFERENCES `car_notify`.`account` (`id`) "\
             "ON DELETE CASCADE "\
             "ON UPDATE NO ACTION)"

    try:
        curs.execute(query0)
        curs.execute(query1)
        curs.execute(query2)
        client.doCommit()
        client.closeConnection()
        return err.OK_CREATION
    except:
        client.doRollback()
        client.closeConnection()
        return err.ERR_404


def dropDatabase(dbName):
    """
    This function is used for drop the database.
    
    :return: A message of success or error. 
    """

    client = sql_client.MySQLClient()
    curs = client.getCursor()

    query = "DROP DATABASE %s"

    try:
        curs.execute(query, (dbName,))
        client.doCommit()
        client.closeConnection()
        return err.OK_DROP
    except:
        client.doRollback()
        client.closeConnection()
        return err.ERR_404


def selectAllFromTable(tableName):
    """
    This function is used for select all the rows from a table. 
    
    :param tableName: The name of the table. 
    :return: A list with all the rows.
    """

    client = sql_client.MySQLClient()
    curs = client.getCursor()

    query = "SELECT * FROM %s;"

    try:
        curs.execute(query, (tableName,))
        return curs.fetchall()
    except pymysql.Error as e:
        client.doRollback()
        client.closeConnection()
        return str(e)

