"""
    This module is a class representing the message send by a car.
    The entity is basically a dictionary that can be easily converter to json and viceversa.
"""
import random
import constant
from time import strftime, sleep
import time
import datetime


class Record:

    def  __init__(self, id=None, speed=None, consumption=None, latitude=None, longitude=None, message=None):

        self.dict = {
            "id": id,
            "speed": speed,
            "consumption": consumption,
            "position": {
                "latitude": latitude,
                "longitude": longitude
            },
            "message": message,
            "time": strftime("%Y-%m-%dT%H:%M:%SZ"),
        }


    def getValueFromAttribute(self, attribute):
        """
        This function is used as getter. 
        
        :param attribute: The string of the field 
        :return: The value on success, None otherwise
        """

        if self.dict.get(attribute) != None:
            return self.dict.get(attribute)
        else:
            return None


    def setValueFromAttribute(self, attribute, value):
        """
        This function is used as setter.
        
        :param attribute: The string of the field
        :param value: The value to set
        :return: Nothing
        """

        if attribute == "position":
            self.dict.get("position")["latitude"] = value[0]
            self.dict.get("position")["longitude"] = value[1]

        elif self.dict.get(attribute) != None:
            self.dict[attribute] = value


    def generate_record(self):
        """
        This function is used for generate some random information.
        
        :return: Nothing
        """

        self.setValueFromAttribute("speed", float(format(random.uniform(40.00, 100.00), ".2f")))
        self.setValueFromAttribute("consumption", float(format(random.uniform(10.0, 20.0), ".2f")))
        self.setValueFromAttribute("position", [float(format(random.uniform(41.735656, 41.80), ".6f")), float(format(random.uniform(12.8656498, 12.90), ".6f"))])
        self.setValueFromAttribute("time", time.time())

        MSG_PROB = int(random.random() * 100)
        if int(MSG_PROB) < constant.MESSAGE_PROBABILITY:
            self.dict["message"] = self.getMessage(MSG_PROB)
        else:
            self.dict["message"] = None


    def simulate_path(self, i):
        """
        This function is used for simulate the path of a car using a list of gps coordinates and speed.

        :return:
        """
        positionList = [[12, 14], [13, 15], [14, 16]]
        speedList = [25, 30, 35]

        position = i % len(positionList)

        self.setValueFromAttribute("position", positionList[position])
        self.setValueFromAttribute("speed", speedList[position])
        self.setValueFromAttribute("time", strftime("%Y-%m-%dT%H:%M:%SZ"))

        if random.random() < constant.MESSAGE_PROBABILITY:
            self.setValueFromAttribute("message", "This is a message!")

        print(self.dict)

    def getMessage(self, prob):
        msg = ["Avaria motore!", "Avaria luce fari destra!", "Avaria airbag!", "Avaria freni anteriori!", "Avaria luce stop sinistra!"]
        index = prob % len(msg)

        return msg[index]

if __name__ == "__main__":

    for i in range(0, 2):
        record = Record(1, 0, 0, 0, 0, "")
        record2 = Record(2, 0, 0, 0, 0, "")
        record.generate_record()
        record2.generate_record()

        #print(type(record.getValueFromAttribute('speed')) is float)
        print(record.dict)
        print(record2.dict)
        sleep(0.5)
