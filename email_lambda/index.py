import boto3
import sql_rest
import os

from botocore.exceptions import ClientError


s3 = boto3.client('s3')


def lambda_handler(event, context):

    bucket = event['Records'][0]['s3']['bucket']['name']
    key = event['Records'][0]['s3']['object']['key']

    try:
        response = s3.get_object(Bucket=bucket, Key=key)
        content = response['Body'].read().decode('utf-8')

    except Exception as e:
        print(e)
        print(
            'Error getting object {} from bucket {}.'.format(
                key, bucket))
        raise e

    index = content.index('\n')
    id = int(content[0:index - 1])
    msg = content[index + 1:]

    email = sql_rest.getEmailFromCar(id)
    car = sql_rest.getCar(id)
    
    if email == -1 or car == None:
        print('ID:' + str(id) + '\nMSG:' + msg)
        print('Informations not found!')
        return "Error"

    sendEmail(email, msg, car)
    
    return "OK"

def sendEmail(email, msg, car):

    SENDER = "Car Notify <work.talone@gmail.com>"
    RECIPIENT = email
    
    ACCESS_KEY = os.environ['ACCESS_KEY']
    SECRET_KEY = os.environ['SECRET_KEY']
    REGION_NAME = os.environ['REGION_NAME']

    SUBJECT = car['brand'] + ' ' + car['model'] + ' ' + car['car_license_plate']
    BODY_TEXT = ("You car: " +SUBJECT + "\r\n"
                 "Information: " + msg)
    BODY_HTML = """<html>
    <head></head>
    <body>
      <h1>""" + SUBJECT + """</h1>
      <p>""" + msg + """</p>
    </body>
    </html>
                """
    CHARSET = "UTF-8"

    email_client = boto3.client('ses', region_name=REGION_NAME,
                          aws_access_key_id=ACCESS_KEY,
                          aws_secret_access_key=SECRET_KEY)

    try:
        response = email_client.send_email(
            Destination={
                'ToAddresses': [
                    RECIPIENT,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=SENDER,
        )

    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])