"""

"""
import influx_client
from influxdb.exceptions import InfluxDBClientError

import time
from datetime import datetime, timedelta


def getGroupByPeriod(period):
    """
    This function is used for get the string of the group by for the query.

    :param period: A period string as daily, weekly, monthly. 
    :return: A string with the value for the query.
    """

    if period == "day":
        return "1m"
    elif period == "week":
        return "1h"
    elif period == "month":
        return "1d"
    else:
        return "1m"


def getPeriodByString(period):
    """
    This function is used for get the string of the period for the query.

    :param period: A period string as daily, weekly, monthly. 
    :return: A string with the value for the query.
    """

    if period == "day":
        return "1d"
    elif period == "week":
        return "7d"
    elif period == "month":
        return "30d"
    else:
        return "1d"


def getMaxByStat(id_car, stat, period):
    """
    This function is used for get the maximum values.

    :param id_car: The id_car 
    :param stat: The statistics 
    :param period: The period 
    :return: A list of [timestamp, value] of the stat during the period from the id_car
    """
    curs = influx_client.MyInfluxClient().getCursor()

    group = getGroupByPeriod(period)
    periodValue = getPeriodByString(period)

    myquery = 'SELECT max(' + stat + ') AS max_' + stat + ' FROM car_stats.autogen.car ' \
                                                          'WHERE time > now() - ' + periodValue + ' AND id_car=\'' + str(
        id_car) + '\' GROUP BY time(' + group + ') FILL(none)'

    try:
        result = curs.query(myquery, epoch='s')
    except InfluxDBClientError as e:
        return e.content

    return parseResult(result, stat, 3)


def getMinByStat(id_car, stat, period):
    """
    This function is used for get the minimum values.

    :param id_car: The id_car 
    :param stat: The statistics 
    :param period: The period 
    :return: A list of [timestamp, value] of the stat during the period from the id_car
    """
    curs = influx_client.MyInfluxClient().getCursor()

    group = getGroupByPeriod(period)
    periodValue = getPeriodByString(period)

    myquery = 'SELECT min(' + stat + ') AS min_' + stat + ' FROM car_stats.autogen.car ' \
                                                          'WHERE time > now() - ' + periodValue + ' AND id_car=\'' + str(
        id_car) + '\' GROUP BY time(' + group + ') FILL(none)'

    try:
        result = curs.query(myquery, epoch='s')
    except InfluxDBClientError as e:
        return e.content

    return parseResult(result, stat, 2)


def getAvgByStat(id_car, stat, period):
    """
    This function is used for get the mean values.

    :param id_car: The id_car 
    :param stat: The statistics 
    :param period: The period 
    :return: A list of [timestamp, value] of the stat during the period from the id_car
    """
    curs = influx_client.MyInfluxClient().getCursor()

    group = getGroupByPeriod(period)
    periodValue = getPeriodByString(period)

    myquery = 'SELECT mean(' + stat + ') AS mean_' + stat + ' FROM car_stats.autogen.car ' \
                                                            'WHERE time > now() - ' + periodValue + ' AND id_car=\'' + str(
        id_car) + '\' GROUP BY time(' + group + ') FILL(0)'

    try:
        result = curs.query(myquery, epoch='s')
    except InfluxDBClientError as e:
        return e.content

    return parseResult(result, stat, 1)


def getSingleValueByStat(id_car, stat, period):
    """
    This function is used for get the single values (max, avg, min) for the dashboard.

    :param id_car: The id_car 
    :param stat: The statistics 
    :param period: The period 
    :return: An object ready for been transformed into a JSON
    """
    result = getAvgByStat(id_car, stat, period)
    
    if len(result['data']) != 0:
        
        min_value = 1000000000000
        min_time = 0
        max_value = 0
        max_time = 0
        total = 0
        len_data = 0
        
        for elem in result['data']:
            value = float(format(elem['value'], ".2f"))
    
            if min_value > value and value != 0:
                min_value = value
                min_time = elem['time']
    
            if max_value < value:
                max_value = value
                max_time = elem['time']
            
            if value !=0:
                total += value
                len_data +=1
        
        avg_value = total / len_data
    
        return {'max': {'time': max_time, 'value': max_value},
                'min': {'time': min_time, 'value': min_value},
                'avg': {'value': float(format(avg_value, ".2f"))}}
    else:
        
        return {'max': {'time': 0, 'value': 0},
                'min': {'time': 0, 'value': 0},
                'avg': {'value': float(format(0, ".2f"))}}


def getValuesByStat(id_car, stat, period):
    """
    This function is used for get all the values.

    :param id_car: The id_car 
    :param stat: The statistics 
    :param period: The period 
    :return: A list of [timestamp, value] of the stat during the period from the id_car
    """
    curs = influx_client.MyInfluxClient().getCursor()

    periodValue = getPeriodByString(period)

    myquery = 'SELECT ' + stat + ' FROM car_stats.autogen.car ' \
                                 'WHERE time > now() - ' + periodValue + ' AND id_car=\'' + str(id_car) + '\''

    try:
        result = curs.query(myquery, epoch='s')
    except InfluxDBClientError as e:
        return e.content

    return parseResult(result, stat, 0)


def parseResult(result, stat, mean=0):
    """
    This function is used for parse the result from a query into a json format for the dashboard.

    :param result: The result of the query.
    :param stat: The type of the stat: speed, consumption...
    :param mean: The type of the aggregate value: 0=no aggregation, 1=mean, 2=max, 3=min.
    :return: A list of object ready for be transform into a JSON.
    """
    if mean == 0:
        index_name = str(stat)
    elif mean == 1:
        index_name = 'mean_' + str(stat)
    elif mean == 2:
        index_name = 'min_' + str(stat)
    elif mean == 3:
        index_name = 'max_' + str(stat)
    else:
        index_name = str(stat)

    result_points = result.get_points()
    final = []

    for elem in result_points:

        if isinstance(elem[index_name], str):
            value = {
                'time': int(datetime_from_utc_to_local(int(elem['time']))) * 1000,
                'value': elem[index_name]
            }
            final.append(value)
        else:
            value = {
                'time': int(datetime_from_utc_to_local(int(elem['time']))) * 1000,
                'value': float(format(elem[index_name], ".2f"))
            }
            final.append(value)

    return {'data': final}


"""
            TEST
"""


# TODO Function for UTC to LOCAL from user preferences
def datetime_from_utc_to_local(utc_datetime):
    now_timestamp = time.time()
    offset = datetime.fromtimestamp(now_timestamp) - datetime.utcfromtimestamp(now_timestamp)
    return utc_datetime + timedelta.total_seconds(offset)


if __name__ == "__main__":
    # result = getMaxByStat("1", "speed", "daily")
    # result = getSingleValueByStat("1", "speed", "daily")
    #result = getValuesByStat("1", "message", "daily")
    print(result)
