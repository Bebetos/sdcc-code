import json
import influx_rest
import time


def respond(err, res=None):
    return {
        'statusCode': '400' if err else '200',
        'body': err.message if err else json.dumps(res),
        'headers': {
            'Content-Type': 'application/json',
        },
    }


def lambda_handler(event, context):
    
    print(event)

    id_car = event['queryStringParameters']['id_car']
    stat = event['queryStringParameters']['stat']
    period = event['queryStringParameters']['period']

    if event['path'] == '/stat/getvalues':
        result = influx_rest.getValuesByStat(id_car, stat, period)
        return respond(None, result)

    elif event['path'] == '/stat/avg':
        millis = int(round(time.time() * 1000))
        result = influx_rest.getAvgByStat(id_car, stat, period)
        print(int(round(time.time() * 1000)) - millis)
        return respond(None, result)

    elif event['path'] == '/stat/max':
        result = influx_rest.getMaxByStat(id_car, stat, period)
        return respond(None, result)

    elif event['path'] == '/stat/min':
        result = influx_rest.getMinByStat(id_car, stat, period)
        return respond(None, result)
        
    elif event['path'] == '/stat/single':
        result = influx_rest.getSingleValueByStat(id_car, stat, period)
        return respond(None, result)

    elif event['path'] == '/stat/logs':
        result = influx_rest.getValuesByStat(id_car, "message", period)
        return respond(None, result)

    else:
        return respond(ValueError('Unsupported method "{}"'.format(operation)))


