"""
    This module is used for the connection with the MySQL database.
"""
import os
from influxdb import InfluxDBClient
from influxdb.exceptions import InfluxDBClientError

# TODO maybe on Enviroment Variable on AWS
IP_ADDRESS = os.environ['IP_ADDRESS']
PORT = os.environ['PORT']
USER = os.environ['USER']
PASSWORD = os.environ['PASSWORD']
DB_NAME = os.environ['DB_NAME']


class MyInfluxClient:
    def __init__(self):
        self.ip_address = IP_ADDRESS
        self.port = PORT
        self.user = USER
        self.password = PASSWORD
        self.db_name = DB_NAME
        self.client = None


    def getCursor(self):
        """
        This function is used for get the connection and the cursor to the database.

        :return: The cursor on success, an error message on failure
        """
        try:
            self.client = InfluxDBClient(self.ip_address, self.port, self.user, self.password, self.db_name)
        except InfluxDBClientError as e:
            return e.message

        return self.client
